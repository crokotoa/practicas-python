# Para insertar elementos a una lista en una posición predeterminada, hay que usar la función insert, indicar la posición y el elemento

miLista=["María", "Pepe", "Marta", "Antonio"]

miLista.insert(2,"Sandra")

print(miLista[:])
