# Añadir un elemento al diccionario
midiccionario={"Alemania":"Berlín","Francia":"París","Reino Unido":"Londres", "España":"Madrid"}
# Se pone el valor mal a proposito para poder corregirlo posteriormente
midiccionario["Italia"]="Lisboa"
print(midiccionario)
# Se corrige el valor simplemente reescribiendolo
midiccionario["Italia"]="Roma"
print(midiccionario)
