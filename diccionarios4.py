# Asignar los valores de un diccionario a traves de una tupla

tupla=["España", "Francia", "Reino Unido", "Alemania"]

# Creamos el diccionario cogiendo los valores de la tupla

diccionario={tupla[0]:"Madrid",tupla[1]:"París",tupla[2]:"Londres",tupla[3]:"Berlín"}

print(diccionario)
